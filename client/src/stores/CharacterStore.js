import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  state: {
    characters: [],
    options: {},
    errors: '',
    seedInputVisible: false
  },
  mutations: {
    addCharacter: (state, newCharacter) => state.characters.push(newCharacter),
    removeCharacter: (state, payload) => {
      state.characters.splice(payload.characterIndex, 1)
    },
    editCharacter: (state, payload) => {
      Vue.set(state.characters, payload.characterIndex, payload.editedCharacter)
    },
    toggleEditMode: (state, payload) => {
      const character = state.characters[payload.characterIndex]
      const newEditState = !character.editMode
      if (Object.keys(state.options).length > 0) {
        Vue.set(character, 'editMode', newEditState)
      }
    },
    setSeedInputVisible: (state, payload) => {
      state.seedInputVisible = Boolean(payload)
    },
    getOptions: (state, payload) => {
      payload.prenom.all = [
        ...new Set([
          ...payload.prenom.M,
          ...payload.prenom.F,
          ...payload.prenom.X
        ])
      ].sort()
      Vue.set(state, 'options', payload)
    }
  },
  actions: {
    async fetchCharacter({ commit }, seed) {
      const getCharacterURL = (seed) => {
        return process.env.VUE_APP_API + '/character?seed=' + (seed || '')
      }

      axios.get(getCharacterURL(seed)).then((character) => {
        const qrURL =
          process.env.VUE_APP_API +
          '/qr/' +
          (encodeURIComponent(getCharacterURL(character.data.seed)) || '')

        axios.get(qrURL).then((qrDataURI) => {
          character.data.qrDataURI = qrDataURI.data
          commit('addCharacter', character.data)
        })
      })
    },
    async postCharacter({ commit }, payload) {
      const characterUpdateURL = process.env.VUE_APP_API + '/character'

      axios.post(characterUpdateURL, payload.editedCharacter).then(
        (response) => {
          console.log(response)
          commit('editCharacter', payload)
        },
        (error) => {
          console.log(error)
        }
      )
    },
    async resetCharacter({ dispatch }, payload) {
      var seedCharacter = {}
      seedCharacter.seed = payload.seed
      console.log(payload, seedCharacter)
      dispatch('postCharacter', seedCharacter)
    },
    async fetchOptions({ commit }) {
      const getOptionsURL = (seed) => {
        return process.env.VUE_APP_API + '/options'
      }

      axios.get(getOptionsURL()).then((options) => {
        commit('getOptions', options.data)
      })
    }
  },
  getters: {
    getCharacters: (state) => {
      return state.characters
    },
    getCharacter: (state) => (index) => {
      return state.characters[index]
    }
  }
})
