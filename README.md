npm install --global mocha

docker volume create mongodbdata
docker run -p 27017:27017 -v mongodbdata:/data/db --name mongo -d mongo

New characteristics need to be added in the following places:

- As JSON or PNG in server\data
- server\test\fixtures\mockCharacter.js
- server\models\character.model.js
- server\core\data.core.js
- server\core\character.core.js

Also change tests as appropriate.
