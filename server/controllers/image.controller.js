const ImageCore = require('../core/image.core')

let ImageController = () => {
    let ImageFunctions = {}

    ImageFunctions.GetColourImage = (req, res) => {
        try {
            const imageData = ImageCore.getColourImageData(req.params.colour)

            res.writeHead(200, {
                'Content-Type': 'image/png',
            })
            imageData.pipe(res, {
                end: true,
            })
        } catch (err) {
            res.status(500).json({ message: err.message })
        }
    }

    ImageFunctions.GetQRImage = async (req, res) => {
        try {
            const imageData = await ImageCore.getQRImageData(req.params.string)
            res.send(imageData)
        } catch (err) {
            res.status(500).json({ message: err.message })
        }
    }

    return ImageFunctions
}

module.exports = ImageController
