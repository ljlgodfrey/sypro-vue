const DataCore = require('../core/data.core')

let OptionsController = () => {
    let OptionsFunctions = {}

    OptionsFunctions.GetOptions = async (req, res) => {
        try {
            const options = DataCore.getOptions()
            res.status(200).json(options)
        } catch (err) {
            res.status(500).json({ message: err.message })
        }
    }

    return OptionsFunctions
}

module.exports = OptionsController
