const { body, validationResult } = require('express-validator')

const CharacterCore = require('../core/character.core')
const RandomCore = require('../core/random.core')

let CharactersController = (Character) => {
    let CharacterFunctions = {}

    const cleanInternalsFromData = (data) => {
        data = data || {}
        delete data._id
        delete data.id
        delete data.__v
        delete data.seed
        delete data.ancillarySeeds
        return data
    }

    const returnCharacterForSeed = async (seed, Character, postedChanges) => {
        seed = seed ? seed : RandomCore.getRandomSeed()
        postedChanges = cleanInternalsFromData(postedChanges)

        console.log('Returning character for', seed)

        const emptyCharacter = { seed: seed }
        let existingCharacter = await Character.findOne(emptyCharacter)

        let characterToReturn = existingCharacter || emptyCharacter
        characterToReturn = CharacterCore.upsertCharacter(
            characterToReturn,
            Character,
            postedChanges
        )

        if (Object.keys(postedChanges).length > 0) {
            await characterToReturn.save()
        }

        return characterToReturn
    }

    CharacterFunctions.GetCharacter = async (req, res) => {
        try {
            const character = await returnCharacterForSeed(
                req.query.seed,
                Character,
                false
            )
            res.body = character
            res.status(200).json(character)
        } catch (err) {
            res.status(500).json({ message: err.message })
        }
    }

    CharacterFunctions.PostCharacter = async (req, res) => {
        try {
            if (req.body.seed) {
                const character = await returnCharacterForSeed(
                    req.body.seed,
                    Character,
                    req.body
                )
                res.body = character
                res.status(200).json(character)
            } else {
                res.status(500).json({ message: '`seed` was missing.' })
            }
        } catch (err) {
            res.status(500).json({ message: err.message })
        }
    }

    return CharacterFunctions
}

module.exports = CharactersController
