const mongoose = require('mongoose')

let characterSchema = new mongoose.Schema({
    seed: {
        type: String,
        required: true,
    },
    ancillarySeeds: {
        surname: {
            type: String,
            required: true,
        },
        gender: {
            type: String,
            required: true,
        },
        sypro: {
            type: String,
            required: true,
        },
        skin: {
            type: String,
            required: true,
        },
        orientation: {
            type: String,
            required: true,
        },
        partnershipStatus: {
            type: String,
            required: true,
        },
        partners: {
            type: String,
            required: true,
        },
        prenom: {
            type: String,
            required: true,
        },
        address: {
            type: String,
            required: true,
        },
        mbti: {
            type: String,
            required: true,
        },
        height: {
            type: String,
            required: true,
        },
        bmi: {
            type: String,
            required: true,
        },
        bloodType: {
            type: String,
            required: true,
        },
    },
    surname: {
        type: String,
        required: true,
    },
    gender: {
        type: String,
        required: true,
    },
    sypro: {
        type: String,
        required: true,
    },
    skin: {
        type: String,
        required: true,
    },
    orientation: {
        type: String,
        required: true,
    },
    partnershipStatus: {
        type: String,
        required: true,
    },
    partners: {
        type: [
            {
                _id: false,
                prenom: {
                    type: String,
                    required: true,
                },
                surname: {
                    type: String,
                    required: true,
                },
            },
        ],
        required: true,
    },
    prenom: {
        type: String,
        required: true,
    },
    address: {
        type: [String],
        required: true,
        validate: [
            addressValidator,
            '{PATH} should contain an array of exactly two strings',
        ],
    },
    mbti: {
        type: {
            _id: false,
            code: {
                type: String,
                required: true,
            },
            descriptor: {
                type: String,
                required: true,
            },
            description: {
                type: String,
                required: true,
            },
        },
        required: true,
    },
    bmi: {
        type: {
            _id: false,
            value: {
                type: Number,
                required: true,
            },
            category: {
                type: String,
                required: true,
            },
        },
        required: true,
    },
    height: {
        type: Number,
        required: true,
    },
    weight: {
        type: Number,
        required: true,
    },
    bloodType: {
        type: String,
        required: true,
    },
})

characterSchema.methods.toJSON = function () {
    var obj = this.toObject()
    delete obj._id
    delete obj.id
    delete obj.__v
    delete obj.ancillarySeeds
    delete obj.partners._id
    delete obj.partners.id
    return obj
}

characterSchema.index({ seed: 1 }, { unique: true })

function addressValidator(address) {
    return address.length == 2
}

module.exports = mongoose.model('Character', characterSchema)
