const fs = require('fs')
const PNG = require('pngjs').PNG

const clone = require('clone')

let DataCore = {}

DataCore.loadData = () => {
    let data = {}
    data.surname = DataCore.getJSONFile('./data/surname.json').surname
    data.gender = DataCore.getJSONFile('./data/gender.json').gender
    data.orientation = DataCore.getJSONFile(
        './data/orientation.json'
    ).orientation
    data.partnershipStatus = DataCore.getJSONFile(
        './data/partnershipStatus.json'
    ).partnershipStatus
    data.sypro = DataCore.getPNGFile('./data/sypro.png')
    data.skin = DataCore.getPNGFile('./data/skin.png')

    data.prenom = {}
    data.prenom.X = DataCore.getJSONFile(
        './data/androgynousPrenoms.json'
    ).prenoms
    data.prenom.F = [
        ...DataCore.getJSONFile('./data/femininePrenoms.json').prenoms,
        ...data.prenom.X,
    ]
    data.prenom.M = [
        ...DataCore.getJSONFile('./data/masculinePrenoms.json').prenoms,
        ...data.prenom.X,
    ]

    data.address = DataCore.getJSONFile(
        './data/apartmentType.json'
    ).apartmentType

    data.mbti = DataCore.getJSONFile('./data/mbti.json').mbti
    data.height = DataCore.getJSONFile('./data/height.json').height
    data.bmi = DataCore.getJSONFile('./data/bmiCategories.json').bmiCategories
    data.bloodType = DataCore.getJSONFile('./data/bloodType.json').bloodType

    return data
}

// get a JSON file from the data directory
DataCore.getJSONFile = (fileName) => {
    const fileContents = fs.readFileSync(fileName, 'utf8')
    return JSON.parse(fileContents)
}

// get a PNG file from the data directory
DataCore.getPNGFile = (fileName) => {
    const fileContents = fs.readFileSync(fileName)
    return PNG.sync.read(fileContents)
}

DataCore.getOptions = () => {
    let options = clone(DataCore.loadData())

    delete options.address
    delete options.skin
    delete options.sypro

    return options
}

module.exports = DataCore
