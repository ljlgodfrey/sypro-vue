const randomSeed = require('random-seed')
const gaussian = require('gaussian')
const clone = require('clone')

let RandomCore = {}

RandomCore.getRandomSeed = (seed, name) => {
    seed = seed ? seed : randomSeed().string(5)
    return String(RandomCore.getRandomInt(seed + (name || ''), 10000, 99999))
}

RandomCore.getRandomInt = (seed, min, max) => {
    return randomSeed(seed).intBetween(min, max)
}

RandomCore.getValueFromArray = (seed, array) => {
    return array[RandomCore.getRandomInt(seed, 0, array.length - 1)]
}

RandomCore.getValueFromWeightedArray = (seed, array) => {
    let sumOfWeights = array.reduce((previous, current) => {
        return (previous += current.weight)
    }, 0)

    let sum = 0,
        r = randomSeed(seed).floatBetween(0, sumOfWeights)

    for (const item of array) {
        sum += item.weight
        if (r <= sum) return item.value
    }
    return new Error('Weighted array selection failed.')
}

RandomCore.getValueFromPNG = (seed, image) => {
    // choose a random point in the canvas
    x = RandomCore.getRandomInt(seed, 0, image.width - 1)
    y = RandomCore.getRandomInt(seed + '1', 0, image.height - 1)

    // get the pixel data
    var idx = (image.width * y + x) << 2
    var r = image.data[idx]
    var g = image.data[idx + 1]
    var b = image.data[idx + 2]

    return rgbToHex(r, g, b)
}

function rgbToHex(r, g, b) {
    return '#' + colourRGBToHex(r) + colourRGBToHex(g) + colourRGBToHex(b)
}

function colourRGBToHex(c) {
    var hex = c.toString(16)
    return hex.length == 1 ? '0' + hex : hex
}

RandomCore.getPrenom = (seed, gender, data) => {
    let genderRole = gender.slice(-1)
    let prenoms = data[genderRole]
    let result = prenoms[RandomCore.getRandomInt(seed, 0, prenoms.length - 1)]
    return result
}

RandomCore.getPartners = (character, data) => {
    let status = character.partnershipStatus
    let seed = character.ancillarySeeds.partnershipStatus

    let partners = []

    if (!status || status == 'Single') {
        return partners
    }

    let partnershipStatusData = data.partnershipStatus.filter((s) => {
        return s.value == status
    })[0]

    let numberOfPartners = RandomCore.getRandomInt(
        seed,
        partnershipStatusData.minPartners,
        partnershipStatusData.maxPartners
    )

    for (let i = 0; i < numberOfPartners; i++) {
        let partnerGender = {
            gender: RandomCore.choosePartnerGender(
                seed + i,
                character.gender,
                character.orientation,
                data
            ),
        }
        partners.push({
            prenom: RandomCore.chooseCharacterPrenom(
                seed + i,
                data.prenom,
                partnerGender
            ),
            surname: RandomCore.getValueFromArray(seed + i, data.surname),
        })
    }
    return partners
}

RandomCore.chooseCharacterPrenom = (seed, data, character) => {
    return RandomCore.getPrenom(seed, character.gender, data)
}

RandomCore.choosePartnerGender = (seed, gender, orientation, data) => {
    const genderRole = gender.slice(-1)
    let availableGenders = []

    if (genderRole == 'X' || orientation == 'Pan') {
        availableGenders = data.gender
    } else if (orientation == 'Gay') {
        availableGenders = clone(data.gender).filter((option) =>
            option.value.includes(genderRole)
        )
    } else {
        availableGenders = clone(data.gender).filter(
            (option) => !option.value.includes(genderRole)
        )
    }

    return RandomCore.getValueFromWeightedArray(seed, availableGenders)
}

RandomCore.chooseCharacterMBTI = (seed, data, character) => {
    const genderRole = character.gender.slice(-1)
    const result = RandomCore.getValueFromWeightedArray(seed, data[genderRole])
    return result
}

RandomCore.generateCharacterHeight = (seed, data, character) => {
    const genderRole = character.gender.slice(-1)
    const heightData = data[genderRole]
    const distribution = gaussian(heightData.mean, heightData.variance)
    const sample = distribution.ppf(randomSeed(seed).random())
    return Math.round(sample)
}

RandomCore.generateCharacterBMI = (seed, data) => {
    const category = RandomCore.getValueFromWeightedArray(seed, data)
    let bmi = randomSeed(seed).floatBetween(category.from, category.to)
    bmi = Math.round(bmi * 10) / 10 //sneaky round to 1dp
    return {
        category: category.description,
        value: bmi,
    }
}

module.exports = RandomCore
