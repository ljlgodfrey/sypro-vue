var PNG = require('pngjs2').PNG
var QRCode = require('qrcode')

let ImageCore = {}

ImageCore.getColourImageData = (colourText) => {
    var rgbColour = colourHexToRGB(colourText)
    var size = 50
    var image = new PNG({
        width: size,
        height: size,
    })

    for (var y = 0; y < image.height; y++) {
        for (var x = 0; x < image.width; x++) {
            var idx = (image.width * y + x) << 2

            var col =
                (x < image.width >> 1) ^ (y < image.height >> 1) ? 0xe5 : 0xff

            image.data[idx] = rgbColour.r
            image.data[idx + 1] = rgbColour.g
            image.data[idx + 2] = rgbColour.b
            image.data[idx + 3] = 0xff
        }
    }

    return image.pack()
}

let colourHexToRGB = (hex) => {
    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
    return result
        ? {
              r: parseInt(result[1], 16),
              g: parseInt(result[2], 16),
              b: parseInt(result[3], 16),
          }
        : null
}

ImageCore.getQRImageData = (string) => {
    return QRCode.toDataURL(string, { margin: 0 })
        .then((url) => {
            return url
        })
        .catch((err) => {
            console.error(err)
        })
}

module.exports = ImageCore
