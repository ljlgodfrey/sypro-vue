const RandomCore = require('../core/random.core')
const getRandomInt = RandomCore.getRandomInt
const getValueFromWeightedArray = RandomCore.getValueFromWeightedArray

let CubieCore = {}

CubieCore.chooseCharacterAddress = (seed, data, character) => {
    return CubieCore.getAddressString(seed, character.partnershipStatus, data)
}

CubieCore.getAddressString = (seed, partnershipStatus, data) => {
    var hex, section, building, floor, cubie
    hex = getRandomInt(seed + 'hex', 0, 6)
    section = getRandomInt(seed + 'section', 1, 6)
    building =
        section % 2
            ? getRandomInt(seed + 'building', 1, 7)
            : getRandomInt(seed + 'building', 1, 17)
    floor = getRandomInt(seed + 'floor', 1, 50)
    cubie = getRandomInt(
        seed + 'cubie',
        1,
        getNumberOfCubiesInBuilding(section, building)
    )
    var apartmentType = getApartmentType(seed + 'type', partnershipStatus, data)
    var result = [
        cubie + apartmentType + '/' + building + '-' + floor,
        'Hex ' + hex + ', Section ' + section,
    ]
    return result
}

let getNumberOfCubiesInBuilding = (section, building) => {
    let result
    if (section % 2 == 0) {
        switch (building) {
            case 1:
            case 17:
                result = 14
                break
            case 2:
            case 16:
                result = 28
                break
            case 3:
            case 15:
                result = 40
                break
            default:
                result = 50
        }
    } else {
        switch (building) {
            case 1:
                result = 82
                break
            case 2:
                result = 90
                break
            case 3:
                result = 100
                break
            case 4:
                result = 108
                break
            case 5:
                result = 116
                break
            case 6:
                result = 124
                break
            default:
                result = 134
        }
    }
    return result
}

let getApartmentType = (seed, partnershipStatus, data) => {
    if (partnershipStatus == 'Single') {
        return getValueFromWeightedArray(seed, data.single)
    } else {
        return getValueFromWeightedArray(seed, data.other)
    }
}

module.exports = CubieCore
