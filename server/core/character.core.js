const RandomCore = require('./random.core')
const DataCore = require('./data.core')
const getRandomSeed = RandomCore.getRandomSeed
const getValueFromArray = RandomCore.getValueFromArray
const getValueFromWeightedArray = RandomCore.getValueFromWeightedArray
const getValueFromPNG = RandomCore.getValueFromPNG
const getPartners = RandomCore.getPartners
const chooseCharacterPrenom = RandomCore.chooseCharacterPrenom
const chooseCharacterMBTI = RandomCore.chooseCharacterMBTI
const generateCharacterHeight = RandomCore.generateCharacterHeight
const generateCharacterBMI = RandomCore.generateCharacterBMI
const CubieCore = require('./cubie.core')
const chooseCharacterAddress = CubieCore.chooseCharacterAddress

let CharacterCore = {}

CharacterCore.upsertCharacter = (
    character,
    CharacterModel,
    newCharacteristics
) => {
    if (!(character instanceof CharacterModel)) {
        character = new CharacterModel(character)
    }

    data = DataCore.loadData()

    if (newCharacteristics) {
        for (const characteristic in newCharacteristics) {
            character[characteristic] = newCharacteristics[characteristic]
        }
    }

    // Set base characteristics
    character.seed = character.seed || getRandomSeed()

    const characteristicsToUpsert = [
        {
            property: 'surname',
            function: getValueFromArray,
        },
        {
            property: 'gender',
            function: getValueFromWeightedArray,
        },
        {
            property: 'sypro',
            function: getValueFromPNG,
        },
        {
            property: 'skin',
            function: getValueFromPNG,
        },
        {
            property: 'orientation',
            function: getValueFromWeightedArray,
        },
        {
            property: 'partnershipStatus',
            function: getValueFromWeightedArray,
        },
        {
            property: 'prenom',
            function: chooseCharacterPrenom,
        },
        {
            property: 'address',
            function: chooseCharacterAddress,
        },
        {
            property: 'mbti',
            function: chooseCharacterMBTI,
        },
        {
            property: 'height',
            function: generateCharacterHeight,
        },
        {
            property: 'bmi',
            function: generateCharacterBMI,
        },
        {
            property: 'bloodType',
            function: getValueFromWeightedArray,
        },
    ]

    for (const characteristic of characteristicsToUpsert) {
        character = CharacterCore.upsertCharacteristic(
            character,
            characteristic.property,
            data,
            characteristic.function
        )
    }

    ensureAncillarySeed(character, 'partners')
    character.partners = getPartners(character, data)
    character.weight = getWeightFromBMIandHeight(
        character.bmi,
        character.height
    )

    return character
}

CharacterCore.upsertCharacteristic = (
    character,
    property,
    data,
    selectionFunction
) => {
    // Ensure derivative data has a seed stored
    character = ensureAncillarySeed(character, property)

    // Then ensure that the property's value is set based on that stored seed, from the available data
    if (!character[property] || character[property].length == 0) {
        character[property] = selectionFunction(
            character.ancillarySeeds[property],
            data[property],
            character
        )
    }

    return character
}

const ensureAncillarySeed = (character, property) => {
    if (!character.ancillarySeeds[property]) {
        character.ancillarySeeds[property] = getRandomSeed(
            character.seed,
            property
        )
    }
    return character
}

const getWeightFromBMIandHeight = (bmi, height) => {
    height = height / 100
    const result = bmi.value * (height * height)
    return Math.round(result * 10) / 10 //sneaky round to 1dp
}

module.exports = CharacterCore
