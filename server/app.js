require('dotenv').config()
const express = require('express')
const app = express()
const cors = require('cors')
const errorhandler = require('errorhandler')
const morgan = require('morgan')

var isProduction = process.env.NODE_ENV === 'production'

app.use(express.json())
app.use(cors())

morgan.token('seed', function (req, res) {
    return req.query.seed || req.body.seed || (res.body && res.body.seed) || ''
})
app.use(
    morgan(
        ':method :url :seed :status :res[content-length] - :response-time ms'
    )
)

if (!isProduction) {
    app.use(errorhandler())
}

app.get('/', (req, res) => {
    res.send('Hello World!')
})

require('./routers/character.router')(app)
require('./routers/options.router')(app)
require('./routers/image.router')(app)

/// catch 404 and forward to error handler
app.use((req, res, next) => {
    var err = new Error('Not Found')
    err.status = 404
    next(err)
})

// error handler
// no stacktraces leaked to user unless in development environment
app.use(function (err, req, res, next) {
    res.status(err.status || 500)
    res.json({
        message: err.message,
        error: !isProduction ? err : {},
    })
})

module.exports = app
