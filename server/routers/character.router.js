const Character = require('../models/character.model')
const CharacterController = require('../controllers/character.controller')(
    Character
)

module.exports = function (app) {
    app.get('/character', CharacterController.GetCharacter)
    app.post('/character', CharacterController.PostCharacter)
}
