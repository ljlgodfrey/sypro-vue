const OptionsController = require('../controllers/options.controller')()

module.exports = function (app) {
    app.get('/options', OptionsController.GetOptions)
}
