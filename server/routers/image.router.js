const ImageController = require('../controllers/image.controller')()

module.exports = function (app) {
    app.get('/image/:colour.png', ImageController.GetColourImage)
    app.get('/qr/:string', ImageController.GetQRImage)
}
