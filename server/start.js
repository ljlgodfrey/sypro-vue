const app = require('./app.js')
const db = require('./models/index.js')
const port = process.env.API_PORT

app.listen(port, () =>
    console.log(`Server app listening at http://localhost:${port}`)
)
