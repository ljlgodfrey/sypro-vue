const chai = require('chai')
const expect = chai.expect

const sinon = require('sinon')
const sinonChai = require('sinon-chai')
chai.use(sinonChai)

const clone = require('clone')

const { mockReq, mockRes } = require('sinon-express-mock')

const mongoose = require('mongoose')
const CharacterController = require('../controllers/character.controller')
const Character = require('../models/character.model')

const mockCharacter = require('./fixtures/mockCharacter')

describe('GET Character', () => {
    afterEach(() => {
        sinon.restore()
    })

    it('should return a previously saved or generated Character in a JSON object', async () => {
        sinon.stub(Character, 'findOne').returns(undefined)
        let saveFunction = sinon.stub(Character.prototype, 'save').returns(true)

        const request = {
            query: { seed: '1234' },
        }
        const req = mockReq(request)
        const res = mockRes()

        await CharacterController(Character).GetCharacter(req, res)

        expect(res.status).to.be.calledWith(200)
        const responseBody = res.json.args[0][0]
        expect(responseBody._id).to.exist
        expect(responseBody.seed).to.exist
        expect(responseBody.surname).to.exist

        expect(saveFunction).to.not.have.been.called
    })

    it('should return a 500 error and an error message if there was a generic problem', async () => {
        sinon.stub(Character, 'findOne').throws(new Error('Oops!'))

        const request = {
            body: {},
        }
        const req = mockReq(request)
        const res = mockRes()

        await CharacterController(Character).GetCharacter(req, res)

        expect(res.status).to.be.calledWith(500)
        expect(res.json).to.be.calledWith({ message: 'Oops!' })
    })
})

describe('POST Character', () => {
    let db

    beforeAll(async () => {
        await mongoose.connect(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        })
        db = mongoose.connection
    })

    afterAll(async () => {
        await db.close()
    })

    beforeEach(async () => {
        await db.collection('characters').deleteMany({})
    })

    afterEach(() => {
        sinon.restore()
    })

    it('should return a previously saved or generated Character in a JSON object', async () => {
        sinon.stub(Character, 'findOne').returns(undefined)
        let saveFunction = sinon.stub(Character.prototype, 'save').returns(true)

        const request = {
            body: clone(mockCharacter),
        }
        const req = mockReq(request)
        const res = mockRes()

        await CharacterController(Character).PostCharacter(req, res)

        expect(res.status).to.be.calledWith(200)
        const responseBody = res.json.args[0][0]
        expect(responseBody._id).to.exist
        expect(responseBody.seed).to.exist
        expect(responseBody.surname).to.exist

        expect(saveFunction).to.have.been.called
    })

    it('should return a 500 error and an error message if there was a generic problem', async () => {
        sinon.stub(Character, 'findOne').throws(new Error('Oops!'))

        const request = {
            body: clone(mockCharacter),
        }
        const req = mockReq(request)
        const res = mockRes()

        await CharacterController(Character).PostCharacter(req, res)

        expect(res.status).to.be.calledWith(500)
        expect(res.json).to.be.calledWith({ message: 'Oops!' })
    })

    it('should return a 500 error and an error message if the seed was missing', async () => {
        sinon.stub(Character, 'findOne').returns({})

        const request = {
            body: {},
        }
        const req = mockReq(request)
        const res = mockRes()

        await CharacterController(Character).PostCharacter(req, res)

        expect(res.status).to.be.calledWith(500)
        expect(res.json).to.be.calledWith({ message: '`seed` was missing.' })
    })

    it('should make and save changes posted into existing Characters', async () => {
        const originalCharacterData = clone(mockCharacter)
        let originalCharacter = await new Character(
            originalCharacterData
        ).save()

        expect(originalCharacter._id).to.exist

        let newCharacterData = clone(mockCharacter)
        newCharacterData.surname = 'Butel'

        const request = {
            body: newCharacterData,
        }
        const req = mockReq(request)
        const res = mockRes()

        await CharacterController(Character).PostCharacter(req, res)
        const updatedCharacter = await Character.findOne()

        expect(res.status).to.be.calledWith(200)
        expect(updatedCharacter.id).to.equal(originalCharacter.id)
        expect(updatedCharacter.seed).to.equal(originalCharacter.seed)
        expect(updatedCharacter.surname).to.equal('Butel')
    })
})
