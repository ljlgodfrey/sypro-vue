const chai = require('chai')
const expect = chai.expect
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const clone = require('clone')

const Character = require('../models/character.model')
const CharacterCore = require('../core/character.core')
const upsertCharacter = CharacterCore.upsertCharacter
const upsertCharacteristic = CharacterCore.upsertCharacteristic

const RandomCore = require('../core/random.core')

const mockCharacter = require('./fixtures/mockCharacter')

describe('Upsert a new Character', () => {
    it('Passing the wrong type of object returns a new Character', () => {
        const inputCharacter = null

        const outputCharacter = upsertCharacter(inputCharacter, Character)

        expect(outputCharacter.constructor.modelName).to.equal(
            Character.modelName
        )
        expect(outputCharacter.isNew).to.be.true
    })

    it('Passing a Character with no seed returns one with a seed', () => {
        const inputCharacter = null

        const outputCharacter = upsertCharacter(inputCharacter, Character)

        expect(outputCharacter.seed).to.match(/[0-9]{5}/)
    })

    it('Passing an existing Character returns a character with the same seed', () => {
        const outputCharacter = upsertCharacter(mockCharacter, Character)

        expect(outputCharacter.seed).to.equal(mockCharacter.seed)
    })
})

describe('Upsert a Characteristic', () => {
    it('When adding a new Characteristic, both the seed and the value are set', () => {
        let character = clone(mockCharacter)
        character.seed = 'test'
        delete character.surname
        delete character.ancillarySeeds.surname

        const data = { surname: ['Godfrey', 'Morgan'] }

        character = upsertCharacteristic(
            character,
            'surname',
            data,
            RandomCore.getValueFromArray
        )

        expect(character.ancillarySeeds.surname).to.equal('50306')
        expect(character.surname).to.equal('Morgan')
    })
})
