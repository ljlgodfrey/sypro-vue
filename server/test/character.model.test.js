const chai = require('chai')
const expect = chai.expect
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const clone = require('clone')

const mongoose = require('mongoose')
const Character = require('../models/character.model')
const mockCharacter = require('./fixtures/mockCharacter')

describe('Create a Character', () => {
    let db

    beforeAll(async () => {
        await mongoose.connect(process.env.MONGO_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true,
        })
        db = mongoose.connection
    })

    afterAll(async () => {
        await db.close()
    })

    beforeEach(async () => {
        await db.collection('characters').deleteMany({})
    })

    it('Create and Save a Character successfully', async () => {
        const validCharacter = new Character(mockCharacter)
        const savedCharacter = await validCharacter.save()

        // Object Id should be defined when successfully saved to MongoDB.
        expect(savedCharacter._id).to.exist
        expect(savedCharacter.seed).to.equal(mockCharacter.seed)
        expect(savedCharacter.surname).to.equal(mockCharacter.surname)
    })

    it('If a Character is missing a seed, it fails validation', async () => {
        let invalidData = clone(mockCharacter)
        delete invalidData.seed

        const invalidCharacter = new Character(invalidData)
        expect(invalidCharacter.save()).to.eventually.be.rejectedWith(
            'Character validation failed: seed: Path `seed` is required.'
        )
    })

    it('If a second Character uses the same seed, it fails validation', async () => {
        let firstCharacter = clone(mockCharacter)
        firstCharacter.seed = 'abcd'
        const validCharacter = new Character(firstCharacter)
        const savedCharacter = await validCharacter.save()

        let secondCharacter = clone(mockCharacter)
        secondCharacter.seed = 'abcd'
        const invalidCharacter = new Character(secondCharacter)

        expect(savedCharacter._id).to.exist
        await expect(invalidCharacter.save()).to.be.rejectedWith(
            'E11000 duplicate key error dup key: { : "abcd" }'
        )
    })
})
