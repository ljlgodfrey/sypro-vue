const chai = require('chai')
const expect = chai.expect
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const sinon = require('sinon')
const sinonChai = require('sinon-chai')
chai.use(sinonChai)

const RandomCore = require('../core/random.core')
const getRandomSeed = RandomCore.getRandomSeed
const getValueFromArray = RandomCore.getValueFromArray
const getValueFromWeightedArray = RandomCore.getValueFromWeightedArray
const getValueFromPNG = RandomCore.getValueFromPNG
const getPrenom = RandomCore.getPrenom
const getPartners = RandomCore.getPartners
const choosePartnerGender = RandomCore.choosePartnerGender
const generateCharacterHeight = RandomCore.generateCharacterHeight
const generateCharacterBMI = RandomCore.generateCharacterBMI

const DataCore = require('../core/data.core')
const data = DataCore.loadData()

describe('Generate a new seed', () => {
    it('New seeds are 5-digit strings', () => {
        const seedValue = getRandomSeed()

        expect(seedValue).to.match(/[0-9]{5}/)
        expect(seedValue).to.be.a('string')
    })

    it('When passed a seed, the result is predictable', () => {
        const seedValue = getRandomSeed('test')

        expect(seedValue).to.equal('21546')
    })

    it('When no seed is passed, the result is unpredictable', () => {
        const firstSeed = getRandomSeed()
        const secondSeed = getRandomSeed()

        expect(firstSeed).not.to.equal(secondSeed)
    })

    it('When passed a seed and a modifier, it results in a modified seed', () => {
        const firstSeedValue = getRandomSeed('test')
        const secondSeedValue = getRandomSeed('test', 1)

        expect(firstSeedValue).not.to.equal(secondSeedValue)
    })
})

describe('Select a predictable value from a simple array', () => {
    it('When passed a seed, the result is predictable', () => {
        const array = ['A', 'B']
        const seed = '1234'

        const returnedValue = getValueFromArray(seed, array)
        expect(returnedValue).to.equal('A')
    })
})

describe('Select a predictable value from a weighted array', () => {
    it('When passed a seed, the result is predictable', () => {
        const array = [
            { weight: 0.5, value: 'A' },
            { weight: 0.5, value: 'B' },
        ]
        const seed = '1234'

        const returnedValue = getValueFromWeightedArray(seed, array)
        expect(returnedValue).to.equal('A')
    })
})

describe('Select a predictable colour value from a PNG', () => {
    it('When passed a seed, the result is predictable', () => {
        const png = DataCore.getPNGFile('./test/fixtures/png.png')
        const seed = '1212'

        const returnedValue = getValueFromPNG(seed, png)
        expect(returnedValue).to.equal('#000000')
    })
})

describe('Select a predictable name from the lists of prenoms', () => {
    it('When passed a seed, the result is predictable', () => {
        const data = { F: ['Susie', 'Phoebe', 'Alex', 'Blair'] }
        const seed = 'zzzz'

        const returnedValue = getPrenom(seed, 'F', data)
        expect(returnedValue).to.equal('Phoebe')
    })
})

describe('Generate partners', () => {
    afterEach(() => {
        sinon.restore()
    })

    it('When passed a seed, generate predictable parters', () => {
        const character = {
            seed: 'zzzz',
            gender: 'cM',
            orientation: 'Pan',
            partnershipStatus: 'Polyamorous',
            ancillarySeeds: { partnershipStatus: 'xxxx' },
        }

        const returnedValue = getPartners(character, data)
        expect(returnedValue[0].prenom).to.equal('Angel')
        expect(returnedValue[1].surname).to.equal('Singh (सिन्गः)')
    })

    it('When choosing a partner for a Pan character, ensure they can be any gender', () => {
        let spiedParameters = sinon.spy(RandomCore, 'getValueFromWeightedArray')
        const seed = 'abcd',
            gender = 'tF',
            orientation = 'Pan'

        const returnedValue = choosePartnerGender(
            seed,
            gender,
            orientation,
            data
        )
        expect(returnedValue).to.equal('cF')
        expect(spiedParameters.args[0][1]).to.have.lengthOf(5)
    })

    it('When choosing a partner for an X character, ensure they can be any gender', () => {
        let spiedParameters = sinon.spy(RandomCore, 'getValueFromWeightedArray')
        const seed = 'abcd',
            gender = 'tX',
            orientation = 'Het'

        const returnedValue = choosePartnerGender(
            seed,
            gender,
            orientation,
            data
        )
        expect(returnedValue).to.equal('cF')
        expect(spiedParameters.args[0][1]).to.have.lengthOf(5)
    })

    it('When choosing a partner for a Gay character, ensure they are the same gender as the character', () => {
        let spiedParameters = sinon.spy(RandomCore, 'getValueFromWeightedArray')
        const seed = 'abcd',
            gender = 'cM',
            orientation = 'Gay'

        const returnedValue = choosePartnerGender(
            seed,
            gender,
            orientation,
            data
        )
        expect(returnedValue).to.equal('cM')
        expect(spiedParameters.args[0][1]).to.deep.equal([
            { value: 'cM', weight: 0.425 },
            { value: 'tM', weight: 0.05 },
        ])
    })

    it('When choosing a partner for a Het character, ensure they are not the same gender', () => {
        let spiedParameters = sinon.spy(RandomCore, 'getValueFromWeightedArray')
        const seed = 'abcd',
            gender = 'tF',
            orientation = 'Het'

        const returnedValue = choosePartnerGender(
            seed,
            gender,
            orientation,
            data
        )
        expect(returnedValue).to.equal('cM')
        expect(spiedParameters.args[0][1]).to.deep.equal([
            { value: 'cM', weight: 0.425 },
            { value: 'tM', weight: 0.05 },
            { value: 'tX', weight: 0.05 },
        ])
    })

    it('When choosing a partner for an Ace character, ensure they are not the same gender', () => {
        let spiedParameters = sinon.spy(RandomCore, 'getValueFromWeightedArray')
        const seed = 'abcd',
            gender = 'tF',
            orientation = 'Ace'

        const returnedValue = choosePartnerGender(
            seed,
            gender,
            orientation,
            data
        )
        expect(returnedValue).to.equal('cM')
        expect(spiedParameters.args[0][1]).to.deep.equal([
            { value: 'cM', weight: 0.425 },
            { value: 'tM', weight: 0.05 },
            { value: 'tX', weight: 0.05 },
        ])
    })
})

describe('Generate character heights', () => {
    it('When passed a seed, generate predictable heights', () => {
        const seed = '0987'
        const character = { gender: 'tX' }
        const result = generateCharacterHeight(seed, data.height, character)

        expect(result).to.equal(167)
    })
})

describe('Generate character BMI', () => {
    it('When passed a seed, generate predictable parters', () => {
        const seed = '7890'
        const character = { gender: 'tX' }
        const result = generateCharacterBMI(seed, data.bmi, character)

        expect(result).to.deep.equal({
            category: 'Underweight Class I',
            value: 16.1,
        })
    })
})
