const chai = require('chai')
const expect = chai.expect
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const DataCore = require('../core/data.core')

describe('Load all required characteristic data', () => {
    it('loadData() returns all required data', async () => {
        const data = DataCore.loadData()

        expect(data.surname[0]).to.be.a('string')

        expect(data.gender[0]).to.be.an('object')
        expect(data.gender[0].weight).to.exist
        expect(data.gender[0].value).to.exist

        expect(data.orientation[0]).to.be.an('object')
        expect(data.orientation[0].weight).to.exist
        expect(data.orientation[0].value).to.exist

        expect(data.sypro).to.be.an('object')
        expect(data.sypro.height).to.exist
        expect(data.sypro.width).to.exist
        expect(data.sypro.data).to.exist

        expect(data.skin).to.be.an('object')
        expect(data.skin.height).to.exist
        expect(data.skin.width).to.exist
        expect(data.skin.data).to.exist

        expect(data.partnershipStatus[0]).to.be.an('object')
        expect(data.partnershipStatus[0].value).to.exist
        expect(data.partnershipStatus[0].minPartners).to.exist
        expect(data.partnershipStatus[0].maxPartners).to.exist

        expect(data.prenom.F[0]).to.be.a('string')
        expect(data.prenom.M[0]).to.be.a('string')
        expect(data.prenom.X[0]).to.be.a('string')

        expect(data.address.single[0]).to.be.an('object')
        expect(data.address.other[0]).to.be.an('object')

        expect(data.mbti.F[0]).to.be.an('object')
        expect(data.mbti.M[0]).to.be.an('object')
        expect(data.mbti.X[0]).to.be.an('object')

        expect(data.height.F).to.be.an('object')
        expect(data.height.M).to.be.an('object')
        expect(data.height.X).to.be.an('object')

        expect(data.bmi[0]).to.be.an('object')

        expect(data.bloodType[0]).to.be.an('object')
    })
})
