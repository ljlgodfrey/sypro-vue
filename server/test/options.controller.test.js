const chai = require('chai')
const expect = chai.expect

const sinon = require('sinon')
const sinonChai = require('sinon-chai')
chai.use(sinonChai)

const DataCore = require('../core/data.core')
const OptionsController = require('../controllers/options.controller')()

const { mockReq, mockRes } = require('sinon-express-mock')

describe('GET Options', () => {
    afterEach(() => {
        sinon.restore()
    })

    it('Should return a JSON object', () => {
        sinon.stub(DataCore, 'getOptions').returns({})

        const req = mockReq()
        const res = mockRes()

        OptionsController.GetOptions(req, res)

        expect(res.status).to.be.calledWith(200)
        const responseBody = res.json.args[0][0]
        expect(responseBody).to.be.an('object')
    })

    it('Should return a 500 error and an error message if there was a generic problem', () => {
        sinon.stub(DataCore, 'getOptions').throws(new Error('Oops!'))

        const req = mockReq()
        const res = mockRes()

        OptionsController.GetOptions(req, res)

        expect(res.status).to.be.calledWith(500)
        expect(res.json).to.be.calledWith({ message: 'Oops!' })
    })
})
