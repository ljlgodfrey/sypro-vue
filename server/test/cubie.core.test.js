const chai = require('chai')
const expect = chai.expect
const chaiAsPromised = require('chai-as-promised')
chai.use(chaiAsPromised)

const CubieCore = require('../core/cubie.core')
const getAddressString = CubieCore.getAddressString

const DataCore = require('../core/data.core')
const data = DataCore.loadData()

describe('Generate address', () => {
    it('When passed a seed, the resulting address array is predictable', () => {
        let seed = 'buzz'
        let partnershipStatus = 'Single'

        let address = getAddressString(seed, partnershipStatus, data.address)

        expect(address).to.deep.equal(['48B/1-29', 'Hex 3, Section 5'])
    })
})
